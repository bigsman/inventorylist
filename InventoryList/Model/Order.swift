//
//  Order.swift
//  InventoryList
//
//  Created by Sohel Seedat on 30/09/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit

struct Order {
	var dateCreated: Date
	var order: [InventoryItem: Int]
}
