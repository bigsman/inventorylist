//
//  Count.swift
//  InventoryList
//
//  Created by Sohel Seedat on 02/10/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit

struct Count {
	var id: String
	var dateCreated: Date
	var dateAmended: Date
	var inventory = [CountItem]()
	var itemsCounted: Int
	var completed: Bool
	
	init() {
		self.id = UUID().uuidString
		self.dateCreated = Date()
		self.dateAmended = Date()
		
		for item in FirebaseService.shared.allItems {
			let countItem = CountItem(item: item, quantity: 0, counted: false)
			self.inventory.append(countItem)
		}
		self.itemsCounted = 0
		self.completed = false
		//toJSON()
	}
	
	init(json: [String: Any]) {
		self.id = json["id"] as! String
		self.dateCreated = json["name"] as! Date
		self.dateAmended = json["info"] as! Date
		self.inventory = json["images"] as! [CountItem]
		self.itemsCounted = json["images"] as! Int
		self.completed = json["images"] as! Bool
	}
	
	func toJSON() {
		var inventoryArray = [[String: Any]]()
		
		for (_, item) in self.inventory.enumerated() {
			inventoryArray.append(item.toJSON())
		}
		
		
		
		let countDictionary: [String: Any] = ["id": self.id,
		                       "dateCreated": self.dateCreated.prettyDate(),
		                       "dateAmended": self.dateAmended.prettyDate(),
		                       "inventory": inventoryArray,
		                       "itemsCounted": self.itemsCounted,
		                       "completed": self.completed]
		do {
			
			//Convert to Data
			let jsonData = try JSONSerialization.data(withJSONObject: countDictionary, options: JSONSerialization.WritingOptions.prettyPrinted)
			
			//Convert back to string. Usually only do this for debugging
			if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
				print(JSONString)
			}
			
			//In production, you usually want to try and cast as the root data structure. Here we are casting as a dictionary. If the root object is an array cast as [Any].
			//let json = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String: Any]
			
			
		} catch {
			print(error.localizedDescription)
		}
	}
}

