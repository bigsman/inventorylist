//
//  CountStore.swift
//  InventoryList
//
//  Created by Sohel Seedat on 09/10/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit

class CountStore {
	
	static let sharedInstance = CountStore()
	
	var counts = [Count]()
	
	private init() {
		print("CountStore is Initalised")
	}
	
	// Fetch All Count items
	func getAllCounts() -> [Count] {
		return counts
	}
	
	// Create Count
	func createCount() -> Count {
		let newCount = Count()
		counts.append(newCount)
		return newCount
	}
	
	// Update Count
	func updateCount(count: Count) {
		for (index, countObject) in self.counts.enumerated() {
			if (countObject.id == count.id) {
				self.counts[index] = count
			}
		}
	}
	
	// delete items
	// sort items
}
