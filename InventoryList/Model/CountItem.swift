//
//  CountItem.swift
//  InventoryList
//
//  Created by Sohel Seedat on 11/10/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit

struct CountItem {
	var item: InventoryItem
	var quantity: Int
	var counted: Bool
	
	func toJSON() -> [String: Any] {
		return ["item": self.item.toJSON(),
		        "quantity": self.quantity,
		        "counted": self.counted]
	}
}
