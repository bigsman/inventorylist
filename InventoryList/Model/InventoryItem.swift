//
//  Item.swift
//  InventoryList
//
//  Created by Sohel Seedat on 12/09/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit
import FirebaseDatabase

enum UnitOfMeasurement: String {
	case Count
	case Grams
	case Tub
	case Bag
	case None
}

enum ItemCategory: String {
	case Food
	case Sauce
	case Drink
	case Packaging
	case Sticker
	case Snacks
	case Uniform
	case Dessert
	case Condiments
	case Others
	case None
}

enum Location: String {
	case Fridge
	case Freezer
	case Backroom
	case Front
	case Kitchen
	case Restaurant
	case Other
}

struct InventoryItem: Hashable {
	
	// Identity
	var id: Int
	var name: String!
	var info: String?
	var images: [String]?
	var active: Bool!
	var productId: String?
	
	// Unit of Measurement
	var packPerSize: Int?
	var packSize: Int?
	var packUOM: UnitOfMeasurement?
	
	// Other
	var category: ItemCategory?
	var location: Location?
	var netPrice: Double?
	var vatIncluded: Bool?
	var notes: String?
		
	var hashValue: Int {
		return id.hashValue ^ name.hashValue
	}
	
	init(snapshot: FIRDataSnapshot) {
		self.id = Int(snapshot.key)!
		
		let json = snapshot.value as! [String: Any]
		self.name = json["name"] as! String
		self.info = json["info"] as? String
		self.images = json["images"] as? [String]
		self.active = json["active"] as? Bool
		self.productId = json["productId"] as? String
		self.packPerSize = json["packPerSize"] as? Int
		self.packSize = json["packSize"] as? Int
		
		if let uom = json["packUOM"] as? String {
			self.packUOM = UnitOfMeasurement(rawValue: uom)
		} else {
			self.packUOM = UnitOfMeasurement.None
		}
		
		if let categoryName = json["category"] as? String {
			self.category = ItemCategory(rawValue: categoryName)!
		} else {
			self.category = ItemCategory.None
		}
		
		if let locationName = json["location"] as? String {
			self.location = Location(rawValue: locationName)
		} else {
			self.location = Location.Other
		}
		
		self.netPrice = json["netPrice"] as? Double
		self.vatIncluded = json["vatIncluded"] as? Bool
		self.notes = json["notes"] as? String
	}
	
	init(json: [String: Any]) {
		self.id = json["id"] as! Int
		self.name = json["name"] as! String
		self.info = json["info"] as? String
		self.images = json["images"] as? [String]
		self.active = json["active"] as? Bool
		self.productId = json["productId"] as? String
		self.packPerSize = json["packPerSize"] as? Int
		self.packSize = json["packSize"] as? Int
		
		if let uom = json["packUOM"] as? String {
			self.packUOM = UnitOfMeasurement(rawValue: uom)
		} else {
			self.packUOM = UnitOfMeasurement.None
		}
		
		if let categoryName = json["category"] as? String {
			self.category = ItemCategory(rawValue: categoryName)!
		} else {
			self.category = ItemCategory.None
		}
		
		if let locationName = json["location"] as? String {
			self.location = Location(rawValue: locationName)
		} else {
			self.location = Location.Other
		}
		
		self.netPrice = json["netPrice"] as? Double
		self.vatIncluded = json["vatIncluded"] as? Bool
		self.notes = json["notes"] as? String
	}
	
	func update() {
		
	}
	
	func toJSON() -> [String: Any?] {
		return ["id": self.id,
		        "name": self.name,
		        "info": self.info,
		        "images": self.images,
		        "active": self.active,
		        "productId": self.productId,
		        "packPerSize": self.packPerSize,
		        "packSize": self.packSize,
		        "packUOM": self.packUOM?.rawValue,
		        "category": self.category?.rawValue,
		        "location": self.location?.rawValue,
		        "netPrice": self.netPrice,
		        "vatIncluded": self.vatIncluded,
		        "notes": self.notes]
	}
}

extension InventoryItem: Equatable {
	static func == (lhs: InventoryItem, rhs: InventoryItem) -> Bool {
		return lhs.id == rhs.id && lhs.name == rhs.name
	}
}
