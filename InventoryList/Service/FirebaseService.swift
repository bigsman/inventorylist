//
//  FirebaseService.swift
//  InventoryList
//
//  Created by Sohel Seedat on 29/09/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Firebase

class FirebaseService {
	
	static let shared = FirebaseService()
	
	let databaseURL = "https://inventorylist-ef190.firebaseio.com/"
	var allItems = [InventoryItem]()
	
	private init() {
		print("Firebase API Initalised")
	}
	
	func updateInventoryItem(id: Int, item: InventoryItem, completion: @escaping (Error?, Bool) -> ()) {
		let endpoint = "/products"
		let databaseRef = FIRDatabase.database().reference(fromURL: databaseURL + endpoint)
		databaseRef.child("\(id)").setValue(item.toJSON()) { (error, databaseRef) in
			if (error == nil) {
				self.getInventoryItems { (allItems) in
					completion(nil, true)
				}
			} else {
				completion(error, false)
			}
		}

	}
	
	func getInventoryItems(completion: @escaping ([InventoryItem]?) -> ()) {
		let endpoint = "/products"
		var items = [InventoryItem]()
		
		let databaseRef = FIRDatabase.database().reference(fromURL: databaseURL + endpoint)
		databaseRef.observeSingleEvent(of: .value, with: {(snapshot) in
			
			for snapshot in snapshot.children.allObjects as! [FIRDataSnapshot] {
				let newItem = InventoryItem(snapshot: snapshot)
				items.append(newItem)
			}

			self.allItems = items
			self.sortItemsByCategories()
			
			completion(items)
		})
	}
	
	func sortItemsByCategories() {
		var sortedItems: [InventoryItem] = []
		
		sortedItems.append(contentsOf: (allItems.filter{$0.category == ItemCategory.Packaging}))
		sortedItems.append(contentsOf: (allItems.filter{$0.category == ItemCategory.Food}))
		sortedItems.append(contentsOf: (allItems.filter{$0.category == ItemCategory.Sauce}))
		sortedItems.append(contentsOf: (allItems.filter{$0.category == ItemCategory.Dessert}))
		sortedItems.append(contentsOf: (allItems.filter{$0.category == ItemCategory.Drink}))
		sortedItems.append(contentsOf: (allItems.filter{$0.category == ItemCategory.Snacks}))
		sortedItems.append(contentsOf: (allItems.filter{$0.category == ItemCategory.Uniform}))
		sortedItems.append(contentsOf: (allItems.filter{$0.category == ItemCategory.Condiments}))
		sortedItems.append(contentsOf: (allItems.filter{$0.category == ItemCategory.Sticker}))
		sortedItems.append(contentsOf: (allItems.filter{$0.category == ItemCategory.Others}))
		sortedItems.append(contentsOf: (allItems.filter{$0.category == ItemCategory.None}))
		
		self.allItems = sortedItems
	}
	
	func getInventoryItemsAsDictionary(completion: @escaping ([[InventoryItem]]?) -> ()) {
		
		self.getInventoryItems { (allItems) in
			if (allItems != nil) {
				var sortedItems: [[InventoryItem]] = []
				
				// Sort items into categories
				sortedItems.append(allItems!.filter{$0.category == ItemCategory.Packaging})
				sortedItems.append(allItems!.filter{$0.category == ItemCategory.Food})
				sortedItems.append(allItems!.filter{$0.category == ItemCategory.Sauce})
				sortedItems.append(allItems!.filter{$0.category == ItemCategory.Dessert})
				sortedItems.append(allItems!.filter{$0.category == ItemCategory.Drink})
				sortedItems.append(allItems!.filter{$0.category == ItemCategory.Snacks})
				sortedItems.append(allItems!.filter{$0.category == ItemCategory.Uniform})
				sortedItems.append(allItems!.filter{$0.category == ItemCategory.Condiments})
				sortedItems.append(allItems!.filter{$0.category == ItemCategory.Sticker})
				sortedItems.append(allItems!.filter{$0.category == ItemCategory.Others})
				sortedItems.append(allItems!.filter{$0.category == ItemCategory.None})
				
				// Remove any empty categories
				sortedItems = sortedItems.filter({ $0.count != 0 })
				
				completion(sortedItems)
			}
		}
	}
	
	func getCounts(completion: @escaping ([Count]?) -> ()) {
		let endpoint = "/counts"
		var counts = [Count]()
		
		let databaseRef = FIRDatabase.database().reference(fromURL: databaseURL + endpoint)
		databaseRef.observeSingleEvent(of: .value, with: {(snapshot) in
			let array = snapshot.value as? [Any]
			
			for product in array! {
				let newCount = Count(json: product as! [String : Any])
				counts.append(newCount)
			}

			completion(counts)
		})
	}

}
