//
//  InventoryViewController.swift
//  InventoryList
//
//  Created by Sohel Seedat on 13/09/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit
import FirebaseDatabase

class InventoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

	// MARK:- IBOutlets

	@IBOutlet weak var tableview: UITableView!
	
	// MARK:- Properties
	
	var items: [[InventoryItem]] = [] {
		didSet {
			self.tableview.reloadData()
		}
	}
	
	// MARK:- Services
	
	let firebaseService = FirebaseService.shared
	
	// MARK:- UIViewController
	
    override func viewDidLoad() {
        super.viewDidLoad()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		loadData()
	}
	
	func loadData() {		
		firebaseService.getInventoryItemsAsDictionary(completion: { (items) in
			if (items != nil) {
				self.items = items!
			}
		})
	}

	// MARK:- UITableView Delegate
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return self.items.count
	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 40
	}
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		let backgroundView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 40))
		backgroundView.backgroundColor = UIColor.darkGray
		
		if let sectionTitle = self.items[section].first?.category?.rawValue {
			let titleLabel = UILabel(frame: CGRect(x: 10, y: 0, width: 150, height: 40))
			titleLabel.text = sectionTitle
			titleLabel.textColor = UIColor.white
			titleLabel.font = UIFont(name: "AvenirNext-Heavy", size: 20)
			backgroundView.addSubview(titleLabel)
			
			return backgroundView
		}
		
		return nil
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.items[section].count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as! ItemTableViewCell
		let inventoryItem = self.items[indexPath.section][indexPath.row]
		cell.configure(item: inventoryItem)
		return cell
	}

	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		if let title = self.items[section].first?.category?.rawValue {
			return title
		} else {
			return nil
		}
	}
	
	// MARK:- Storyboard
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if(segue.identifier == "InventoryDetailID") {
			let detailVC = segue.destination as! InventoryDetailViewController
			detailVC.item = items[tableview.indexPathForSelectedRow!.section][tableview.indexPathForSelectedRow!.row]
			detailVC.selectedIndex = tableview.indexPathForSelectedRow?.row
		}
	}

}
