//
//  CountDetailViewController.swift
//  InventoryList
//
//  Created by Sohel Seedat on 02/10/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit

class CountDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

	// MARK: IBOutlets

	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var dateCreatedLabel: UILabel!
	@IBOutlet weak var dateUpdatedLabel: UILabel!
	@IBOutlet weak var itemsCountedLabel: UILabel!
	@IBOutlet weak var informationLabel: UILabel!
	@IBOutlet weak var countButton: UIButton!
	
	// MARK: Properties

	var count: Count!
	
	// MARK: View Controller
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		loadData()
	}
	
	func loadData() {
		self.dateCreatedLabel.text = "Date Created: \(count.dateCreated.prettyDate())"
		self.dateUpdatedLabel.text = "Last Updated: \(count.dateAmended.prettyDate())"
		self.itemsCountedLabel.text = "Items Counted: \(count.itemsCounted)"
		self.informationLabel.text = count.completed ? "Status: Completed" : "Status: Not Completed"
		
		if (count.completed == false && count.itemsCounted > 0) {
			countButton.setTitle("Continue Count".uppercased(), for: .normal)
		} else if (count.completed == true) {
			countButton.setTitle("Count Completed".uppercased(), for: .normal)
		}
	}
	
	// MARK: TableView Delegate
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return count.inventory.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let countItem = count.inventory[indexPath.row]
		
		let cell = tableView.dequeueReusableCell(withIdentifier: "CountCell", for: indexPath)
		cell.textLabel?.text = "\(countItem.item.name!)"
		cell.detailTextLabel?.text = "\(countItem.quantity)"
		return cell
	}
	
	// MARK: Storyboard
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if (segue.identifier == "CountInventoryID") {
			let vc = segue.destination as! CountInventoryViewController
			vc.count = self.count
		}
	}

}
