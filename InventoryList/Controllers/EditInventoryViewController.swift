//
//  EditInventoryViewController.swift
//  InventoryList
//
//  Created by Sohel Seedat on 28/09/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit
import FirebaseDatabase
import APESuperHUD

protocol EditInventoryViewControllerDelegate: class {
	func inventoryUpdated(newInventoryItem: InventoryItem)
}

class EditInventoryViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

	// MARK:- IBOutlets

	@IBOutlet weak var nameTextField: UITextField!
	@IBOutlet weak var descriptionTextView: UITextView!
	@IBOutlet weak var categoryTextField: UITextField!
	@IBOutlet weak var productIdTextField: UITextField!
	@IBOutlet weak var itemsPerPackTextField: UITextField!
	@IBOutlet weak var itemSizeTextField: UITextField!
	@IBOutlet weak var itemUomTextField: UITextField!
	@IBOutlet weak var locationTextField: UITextField!
	@IBOutlet weak var priceTextField: UITextField!
	@IBOutlet weak var notesTextView: UITextView!
	
	// MARK:- Properties

	var item: InventoryItem!
	var tempItem: InventoryItem!
	var selectedIndex: Int!
	let ref = FIRDatabase.database().reference(fromURL: "https://inventorylist-ef190.firebaseio.com/products")
	let pickerView = UIPickerView()

	let categoryArray = ["Food", "Sauce", "Drink", "Packaging", "Sticker", "Snacks", "Uniform", "Dessert", "Condiments", "Others", "None"]
	let uomArray = ["Count", "Grams", "Tub", "Bag", "None"]
	let locationArray = ["Fridge", "Freezer", "Backroom", "Front", "Kitchen", "Restaurant", "Other"]
	
	weak var delegate: EditInventoryViewControllerDelegate?

	// MARK:- View Controller Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
		
		self.navigationController?.setNavigationBarHidden(true, animated: false)
		
		setupPickerViews()
		loadData()
	}
	
	func setupPickerViews() {
		self.pickerView.delegate = self
		self.pickerView.dataSource = self
		categoryTextField.inputView = self.pickerView
		itemUomTextField.inputView = self.pickerView
		locationTextField.inputView = self.pickerView
	}
	
	// MARK:- PickerView

	func numberOfComponents(in pickerView: UIPickerView) -> Int {
		return 1
	}
	
	func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		if (categoryTextField.isFirstResponder) {
			return categoryArray.count
		} else if (itemUomTextField.isFirstResponder) {
			return uomArray.count
		} else if (locationTextField.isFirstResponder) {
			return locationArray.count
		}
		
		return 0
	}
	
	func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
		if (categoryTextField.isFirstResponder) {
			return categoryArray[row]
		} else if (itemUomTextField.isFirstResponder) {
			return uomArray[row]
		} else if (locationTextField.isFirstResponder) {
			return locationArray[row]
		}
		return nil
	}
	
	func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
		if (categoryTextField.isFirstResponder) {
			categoryTextField.text = categoryArray[row]
		} else if (itemUomTextField.isFirstResponder) {
			itemUomTextField.text = uomArray[row]
		} else if (locationTextField.isFirstResponder) {
			locationTextField.text = locationArray[row]
		}
	}

	func loadData() {
		tempItem = self.item
		
		nameTextField.text = item.name
		
		if let description = item.info {
			descriptionTextView.text = description
		}
		
		if let category = item.category {
			categoryTextField.text = category.rawValue
		}
		
		if let productId = item.productId {
			productIdTextField.text = productId
		}
		
		if let itemsPerPack = item.packPerSize {
			itemsPerPackTextField.text = "\(itemsPerPack)"
		}
		
		if let itemSize = item.packSize {
			itemSizeTextField.text = "\(itemSize)"
		}
		
		if let itemUOM = item.packUOM {
			itemUomTextField.text = itemUOM.rawValue
		}
		
		if let itemLocation = item.location {
			locationTextField.text = itemLocation.rawValue
		}
		
		if let price = item.netPrice {
			priceTextField.text = "\(price)"
		}
		
		if let notes = item.notes {
			notesTextView.text = notes
		}
	}
	
	func textViewDidEndEditing(_ textView: UITextView) {
		guard let text = textView.text, !text.isEmpty else {
			return
		}
		
		switch textView {
		case descriptionTextView:
			tempItem.info = text
		case notesTextView:
			tempItem.notes = text
		default:
			print("Error - no other Text View")
		}
	}
	
	func textFieldDidBeginEditing(_ textField: UITextField) {
		guard let text = textField.text, !text.isEmpty else {
			return
		}
		
		switch textField {
		case categoryTextField:
			let category = ItemCategory(rawValue: text)
			for (index, categoryString) in categoryArray.enumerated() {
				if (category!.rawValue == categoryString) {
					self.pickerView.selectRow(index, inComponent: 0, animated: false)
				}
			}
		case itemUomTextField:
			let uom = UnitOfMeasurement(rawValue: text)
			for (index, uomString) in uomArray.enumerated() {
				if (uom!.rawValue == uomString) {
					self.pickerView.selectRow(index, inComponent: 0, animated: false)
				}
			}
		case locationTextField:
			let location = Location(rawValue: text)
			for (index, locationString) in locationArray.enumerated() {
				if (location!.rawValue == locationString) {
					self.pickerView.selectRow(index, inComponent: 0, animated: false)
				}
			}
		default:
			print("")
		}

	}
	
	func textFieldDidEndEditing(_ textField: UITextField) {
		guard let text = textField.text, !text.isEmpty else {
			return
		}
		
		switch textField {
		case nameTextField:
			tempItem.name = text
		case categoryTextField:
			tempItem.category = ItemCategory(rawValue: text)
		case productIdTextField:
			tempItem.productId = text
		case itemsPerPackTextField:
			tempItem.packPerSize = Int(text)
		case itemSizeTextField:
			tempItem.packSize = Int(text)
		case itemUomTextField:
			tempItem.packUOM = UnitOfMeasurement(rawValue: text)
		case locationTextField:
			tempItem.location = Location(rawValue: text)
		case priceTextField:
			tempItem.netPrice = Double(text)
		default:
			print("Error - no other Text Fields")
		}
	}
	
	// MARK:- IBActions

	@IBAction func cancelButtonPressed(_ sender: Any) {
		self.dismiss(animated: false, completion: nil)
	}

	@IBAction func saveButtonPressed(_ sender: Any) {
		FirebaseService.shared.updateInventoryItem(id: self.item.id, item: tempItem) { (error, success) in
			if (success == true) {
				APESuperHUD.showOrUpdateHUD(title: "Inventory", message: "The inventory item has been updated", presentingView: self.view, completion: {
					self.delegate?.inventoryUpdated(newInventoryItem: self.tempItem)
					self.dismiss(animated: true, completion: nil)
				})
			} else {
				print("ERROR UPDATING")
			}
		}
	}

}
