//
//  CountInventoryViewController.swift
//  InventoryList
//
//  Created by Sohel Seedat on 03/10/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class CountInventoryViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITextFieldDelegate {

	// MARK:- IBOutlets

	@IBOutlet weak var collectionView: UICollectionView!

	// MARK:- Properties

	var count: Count!
	var inventoryItems: [CountItem]!
	
	// MARK:- Services
	
	let countStore = CountStore.sharedInstance

	// MARK:- View Controller

	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.inventoryItems = self.count.inventory
		self.collectionView.shouldIgnoreScrollingAdjustment = true
		self.collectionView.backgroundColor = UIColor.init(gradientStyle: .topToBottom, withFrame: self.view.frame, andColors: [UIColor.flatBlueDark, UIColor.flatMint])
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.navigationController?.setNavigationBarHidden(true, animated: false)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		countStore.updateCount(count: self.count)
		self.navigationController?.setNavigationBarHidden(false, animated: false)
	}
	
	// MARK:- CollectionView Delegate
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return inventoryItems.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let countItem = inventoryItems[indexPath.row]
		
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InventoryItemCellID", for: indexPath) as! CountInventoryCollectionViewCell
		cell.configure(countItem: countItem, indexRow: indexPath.row)
		cell.valueTextField.delegate = self
		cell.valueTextField.addPreviousNextDoneOnKeyboardWithTarget(self,
		                                                            previousAction:  #selector(CountInventoryViewController.toolbarPreviousButtonPressed),
		                                                            nextAction:  #selector(CountInventoryViewController.toolbarNextButtonPressed),
		                                                            doneAction:  #selector(CountInventoryViewController.toolbarDoneButtonPressed))
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return CGSize(width: collectionView.frame.size.width * 0.8, height: collectionView.frame.size.height * 0.5)
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
		let inset: CGFloat = (collectionView.frame.width * 0.5) - ((collectionView.frame.size.width * 0.8) * 0.5)
		return UIEdgeInsetsMake(0, inset, ((collectionView.frame.size.height * 0.5) * 0.5), inset)
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
		return 10
	}
	
	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		let centerOffset = scrollView.contentOffset.x + (scrollView.frame.size.width/2)

		if let proposedIndexPath = self.collectionView.indexPathForItem(at: CGPoint(x: centerOffset, y: ((collectionView.frame.size.height * 0.5) * 0.5))) {
			self.collectionView.scrollToItem(at: proposedIndexPath, at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
		}
	}

	func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
		let currentIndexPath = collectionView.indexPathForItem(at: CGPoint(x: collectionView.center.x + collectionView.contentOffset.x, y: 100))
		let cell = collectionView.cellForItem(at: currentIndexPath!) as! CountInventoryCollectionViewCell
		cell.valueTextField.becomeFirstResponder()
	}
	
	// MARK:- Textfield Delegate
	
	func textFieldDidEndEditing(_ textField: UITextField) {
		var countItem = inventoryItems[textField.tag]
		
		if (textField.text?.characters.count != 0) {
			countItem.quantity = Int(textField.text!)!
			countItem.counted = true

			self.count.inventory[textField.tag] = countItem
			self.inventoryItems = self.count.inventory
			countStore.updateCount(count: self.count)
		}
	}
	
	// MARK:- IQKeyboard Delegate

	func toolbarPreviousButtonPressed() {
		let currentIndexPath = collectionView.indexPathForItem(at: CGPoint(x: collectionView.center.x + collectionView.contentOffset.x, y: 100))
		var previousIndexPath = currentIndexPath
		previousIndexPath!.row -= 1
		collectionView.scrollToItem(at: previousIndexPath!, at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
	}
	
	func toolbarNextButtonPressed() {
		let currentIndexPath = collectionView.indexPathForItem(at: CGPoint(x: collectionView.center.x + collectionView.contentOffset.x, y: 100))
		var nextIndexPath = currentIndexPath
		nextIndexPath!.row += 1
		collectionView.scrollToItem(at: nextIndexPath!, at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
	}
	
	func toolbarDoneButtonPressed() {
		IQKeyboardManager.sharedManager().resignFirstResponder()
	}

	// MARK:- IBActions
	
	@IBAction func backButtonPressed(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func completeButtonPressed(_ sender: Any) {
		self.count.completed = true
	}
	
	// MARK:- Storyboard

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if (segue.identifier == "CountSummaryID") {
			let vc = segue.destination as! CountInventorySummaryViewController
			vc.inventoryItems = self.inventoryItems
		}
	}

}
