//
//  CountInventoryCollectionViewCell.swift
//  InventoryList
//
//  Created by Sohel Seedat on 03/10/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit
import ChameleonFramework

class CountInventoryCollectionViewCell: UICollectionViewCell {
    
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var uomLabel: UILabel!
	@IBOutlet weak var valueTextField: UITextField!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		self.layer.cornerRadius = 5.0
		self.layer.borderWidth = 1.0
		self.layer.borderColor = UIColor.black.cgColor
		
		self.valueTextField.layer.cornerRadius = 5.0
	}
	
	func configure(countItem: CountItem, indexRow: Int) {
		self.nameLabel.text = countItem.item.name
		
		if let uom = countItem.item.packUOM {
			self.uomLabel.text = uom.rawValue
		} else {
			self.uomLabel.text = "Count"
		}
		
		if (countItem.quantity == 0) {
			self.valueTextField.text = ""
		} else {
			self.valueTextField.text = "\(countItem.quantity)"
		}
		self.valueTextField.tag = indexRow
			
		switch countItem.item.category! {
		case .Food:
			self.backgroundColor = UIColor.init(gradientStyle: .topToBottom, withFrame: self.frame, andColors: [UIColor.flatBlue, UIColor.flatBlueDark])
		case .Dessert:
			self.backgroundColor = UIColor.init(gradientStyle: .topToBottom, withFrame: self.frame, andColors: [UIColor.flatMint, UIColor.flatMintDark])
		case .Packaging:
			self.backgroundColor = UIColor.init(gradientStyle: .topToBottom, withFrame: self.frame, andColors: [UIColor.flatRed, UIColor.flatRedDark])
		case .Drink:
			self.backgroundColor = UIColor.init(gradientStyle: .topToBottom, withFrame: self.frame, andColors: [UIColor.flatPlum, UIColor.flatPlumDark])
		case .Sticker:
			self.backgroundColor = UIColor.init(gradientStyle: .topToBottom, withFrame: self.frame, andColors: [UIColor.flatTeal, UIColor.flatTealDark])
		default:
			self.backgroundColor = UIColor.init(gradientStyle: .topToBottom, withFrame: self.frame, andColors: [UIColor.flatGray, UIColor.flatGrayDark])
		}
	}
}
