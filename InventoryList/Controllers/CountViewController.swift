//
//  CountViewController.swift
//  InventoryList
//
//  Created by Sohel Seedat on 02/10/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit

class CountViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

	// MARK: IBOutlets

	@IBOutlet weak var tableView: UITableView!
	
	// MARK: Properties

	var counts = [Count]()
	
	// MARK: Services

	let countStore = CountStore.sharedInstance
	
	// MARK: View Controller

    override func viewDidLoad() {
        super.viewDidLoad()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		self.counts = countStore.getAllCounts()
		self.tableView.reloadData()
	}
	
	// MARK: TableView Delegate

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return counts.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let count = counts[indexPath.row]
		
		let cell = tableView.dequeueReusableCell(withIdentifier: "CountCell", for: indexPath)
		cell.textLabel?.text = "\(count.dateCreated.prettyDate())"
		cell.detailTextLabel?.text = count.completed ? "Completed" : "Not Completed"
		return cell
	}

	// MARK: IBActions

	@IBAction func addButtonPressed(_ sender: Any) {		
		let newCount = countStore.createCount()
		self.counts.insert(newCount, at: 0)
		
		self.tableView.beginUpdates()
		self.tableView.insertRows(at: [IndexPath.init(row: 0, section: 0)], with: .automatic)
		self.tableView.endUpdates()
	}
	
	// MARK: Storyboard
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if (segue.identifier == "CountDetailID") {
			let vc = segue.destination as! CountDetailViewController
			vc.count = self.counts[self.tableView.indexPathForSelectedRow!.row]
		}
	}

}

extension Date {
	func prettyDate() -> String {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "dd MMMM yy"
		return dateFormatter.string(from: self)
	}
	
	func prettyDateNoYear() -> String {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "dd MMMM"
		return dateFormatter.string(from: self)
	}
}
