//
//  InventoryDetailViewController.swift
//  InventoryList
//
//  Created by Sohel Seedat on 20/09/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit

class InventoryDetailViewController: UIViewController, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, EditInventoryViewControllerDelegate {

	// MARK:- IBOutlets

	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var pageControl: UIPageControl!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var infoLabel: UILabel!
	@IBOutlet weak var categoryLabel: UILabel!

	// MARK:- Properties

	var item: InventoryItem!
	var selectedIndex: Int!
	let tableTitlesArray = ["Product ID", "Pack Per Size", "Pack Size", "Pack UOM", "Location", "Price", "Notes"]
	
	// MARK:- UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
		
		self.scrollView.delegate = self

		loadData()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		self.navigationController?.setNavigationBarHidden(true, animated: false)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		self.navigationController?.setNavigationBarHidden(false, animated: false)
	}
	
	func loadData() {
		
		loadImages()
		
		titleLabel.text = item.name
		
		if let info = item.info {
			infoLabel.text = info
		} else {
			infoLabel.text = ""
		}
		
		if let packageType = item.category {
			categoryLabel.text = packageType.rawValue.uppercased()
		}
	}
	
	func loadImages() {
		var colors = [UIColor.flatBlue, UIColor.flatRed, UIColor.flatGreen, UIColor.flatYellow]
		
		if let images = item.images {
			for (index, _) in images.enumerated() {
				var frame = CGRect.zero
				frame.origin.x = self.scrollView.frame.size.width * CGFloat(index)
				frame.size = self.scrollView.frame.size
				
				print(self.view.frame.width)
				print(self.scrollView.frame.width)
				
				let subView = UIView(frame: frame)
				subView.backgroundColor = colors[index]
				self.scrollView.addSubview(subView)
			}
			
			pageControl.numberOfPages = images.count
			
			self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width * CGFloat((item.images?.count)!), height: self.scrollView.frame.size.height)

		} else {
			var frame = CGRect.zero
			frame.origin.x = 0
			frame.size = self.scrollView.frame.size
			
			let subView = UIImageView(image: UIImage(named: "itemImagePlaceholder"))
			subView.frame = frame
			subView.contentMode = .scaleAspectFill
			subView.backgroundColor = UIColor.gray
			self.scrollView.addSubview(subView)
			
			pageControl.isHidden = true
			
			self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height: self.scrollView.frame.size.height)
		}

		self.scrollView.isPagingEnabled = true
	}
	
	// MARK:- UIScrollView Delegate
	
	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		// Test the offset and calculate the current page after scrolling ends
		let pageWidth = CGFloat(scrollView.frame.width)
		let currentPage = Int(scrollView.contentOffset.x / pageWidth)
		self.pageControl.currentPage = currentPage
	}
	
	// MARK:- TableView Delegate
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return tableTitlesArray.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "ItemDetailCell", for: indexPath) as! ItemDetailTableViewCell
		cell.configure(title: tableTitlesArray[indexPath.row], item: self.item, indexRow: indexPath.row)
		return cell
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		if (indexPath.row == 6) {
			return 80
		}
		else {
			return 44
		}
	}

	// MARK:- IBAction

	@IBAction func backButtonPressed(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	// MARK:- Storyboard

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if (segue.identifier == "EditItemViewID") {
			let vc = segue.destination as! EditInventoryViewController
			vc.item = self.item
			vc.selectedIndex = self.selectedIndex
			vc.delegate = self
		}
	}
	
	// MARK:- EditInventoryViewController Delegate
	
	func inventoryUpdated(newInventoryItem: InventoryItem) {
		self.item = newInventoryItem
		loadData()
	}
}

extension Double {
	func toCurrencyString() -> String {
		let formatter = NumberFormatter()
		formatter.numberStyle = .currency
		formatter.maximumFractionDigits = 2
		formatter.locale = Locale(identifier: "en_GB")
		let result = formatter.string(from: NSNumber(value: self))
		return result!
	}
}

