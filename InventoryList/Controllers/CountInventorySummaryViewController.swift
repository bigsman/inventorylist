//
//  CountInventorySummaryViewController.swift
//  InventoryList
//
//  Created by Sohel Seedat on 07/10/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit

class CountInventorySummaryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

	@IBOutlet weak var tableView: UITableView!
	
	var inventoryItems: [CountItem]!
	
    override func viewDidLoad() {
        super.viewDidLoad()

		self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneButtonPressed))
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return inventoryItems.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let countItem = inventoryItems[indexPath.row]
		
		let cell = tableView.dequeueReusableCell(withIdentifier: "CountSummaryCell", for: indexPath)
		cell.textLabel?.text = countItem.item.name
		cell.detailTextLabel?.text = "\(countItem.quantity)"
		return cell
	}
	
	func doneButtonPressed() {
		self.navigationController?.popToRootViewController(animated: true)
	}
	
	@IBAction func backButtonPressed(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}


}
