//
//  InventoryCollectionViewController.swift
//  InventoryList
//
//  Created by Sohel Seedat on 25/09/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit

class InventoryCollectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

	let categories = ["Food", "Sauces", "Drinks", "Packaging", "Stickers", "Snacks", "Uniform", "Desserts", "Condiments", "Others", "None"]
	
	@IBOutlet weak var collectionView: UICollectionView!
	
    override func viewDidLoad() {
        super.viewDidLoad()
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return categories.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryID", for: indexPath) as! CategoryCollectionViewCell
		
		cell.titleLabel.text = categories[indexPath.row]
		cell.imageView.backgroundColor = UIColor.gray
		
		return cell
	}

}
