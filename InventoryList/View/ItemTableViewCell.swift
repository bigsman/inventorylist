//
//  ItemTableViewCell.swift
//  InventoryList
//
//  Created by Sohel Seedat on 18/09/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {

	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var infoLabel: UILabel!
	@IBOutlet weak var categoryLabel: UILabel!
	
	let vatRate = 1.20
	
	// Initialization code
	
    override func awakeFromNib() {
        super.awakeFromNib()
		
		self.contentView.backgroundColor = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 0.6)
		self.selectionStyle = .none
    }

	func configure(item: InventoryItem) {
		self.titleLabel.text = item.name
		
		if let itemCategory = item.info {
			self.infoLabel.text = itemCategory
		} else {
			self.infoLabel.text = ""
		}
		
		if let price = item.netPrice {
			self.categoryLabel.text = "£\(price)"
			
			if let vat = item.vatIncluded {
				self.categoryLabel.text = vat ? formatCurrency(value: (price * vatRate)) : formatCurrency(value: price)
			}
		}
	}
	
	func formatCurrency(value: Double) -> String {
		let formatter = NumberFormatter()
		formatter.numberStyle = .currency
		formatter.maximumFractionDigits = 2
		formatter.locale = Locale(identifier: "en_GB")
		let result = formatter.string(from: NSNumber(value: value))
		return result!
	}

}
