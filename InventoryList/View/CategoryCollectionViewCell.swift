//
//  CategoryCollectionViewCell.swift
//  InventoryList
//
//  Created by Sohel Seedat on 25/09/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
	
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var imageView: UIImageView!
    
}
