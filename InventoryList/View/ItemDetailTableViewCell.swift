//
//  ItemDetailTableViewCell.swift
//  InventoryList
//
//  Created by Sohel Seedat on 21/09/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit

class ItemDetailTableViewCell: UITableViewCell {

	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var valueLabel: UILabel!
	
    override func awakeFromNib() {
        super.awakeFromNib()
		
		// Initialization code
		self.valueLabel.text = "N/A"
    }
	
	func configure(title: String, item: InventoryItem, indexRow: Int) {
		self.titleLabel.text = title
		
		switch indexRow {
		case 0:
			if let productID = item.productId {
				self.valueLabel.text = "\(productID)"
			}
		case 1:
			if let packPerSize = item.packPerSize {
				self.valueLabel.text = "\(packPerSize)"
			}
		case 2:
			if let packSize = item.packSize {
				self.valueLabel.text = "\(packSize)"
			}
		case 3:
			if let packUOM = item.packUOM {
				self.valueLabel.text = "\(packUOM.rawValue)"
			}
		case 4:
			if let location = item.location {
				self.valueLabel.text = "\(location)"
			}
		case 5:
			if let price = item.netPrice {
				self.valueLabel.text = price.toCurrencyString()
			}
		case 6:
			if let notes = item.notes {
				self.valueLabel.text = "\(notes)"
			}
		default:
			print("Not Found")
		}
	
	}

}
