//
//  AppDelegate.swift
//  InventoryList
//
//  Created by Sohel Seedat on 12/09/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit
import Firebase
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?


	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		// Override point for customization after application launch.
		
		FIRApp.configure()

		IQKeyboardManager.sharedManager().enable = true
		
//		UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
		
		let navigationBarAppearace = UINavigationBar.appearance()
		navigationBarAppearace.titleTextAttributes = [NSFontAttributeName: UIFont(name: "AvenirNext-Heavy", size: 24)!]
//		navigationBarAppearace.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
//		navigationBarAppearace.tintColor = UIColor.white
//		navigationBarAppearace.barTintColor = UIColor.flatBlue
		
		return true
	}
}

